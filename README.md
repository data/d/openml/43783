# OpenML dataset: Covid-19-Research-Articles-(NCBI)

https://www.openml.org/d/43783

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
I collected about 1200 Covid-19 research articles from the NCBI.NLM.NIH website to be utilized in ML algorithms/ Data Analysis such as Sentiment Analysis, Time Series, Recommender System and/or Classification. 
Content
link: URL to the research article
title: research article
keywords: words under which the research article is categorized
dates: publication date online
abstract: a brief summary of the article (methods  hypothesis included)
conclusion: findings of the research
**For the sake of time, I left some columns with 'null'  String values. It's your choice to filter the values, and use what is more appropriate for your ML model.
**I didn't include authors/contributors as it won't serve a purpose in this datasets 
Inspiration
I am interested in knowing the focus of those studies (by analyzing word frequencies) as well as analyzing the volume of publications over time.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43783) of an [OpenML dataset](https://www.openml.org/d/43783). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43783/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43783/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43783/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

